import java.util.Scanner;

public class Principal{
        public static void main(String[] args){
        		String nombreUsuario = null;
        		int numeroMaximo = 0;
        		boolean bucle1 = true;
        		boolean bucle2 = false;
        		boolean bucle3 = false;
        		boolean bucle4 = true;
        		int numeroRandom;
        		int intentos = 0;
        		int respuestaUsuario = 0;
        		int numeroIntroducido =0;
                Scanner sc = new Scanner(System.in);
                
                
                System.out.println("/////////////////////////////////////////////////////////////////////////"+"\n/////////El juego ha comenzado introduce tu nombre para comenzar/////////" + "\n/////////////////////////////////////////////////////////////////////////");
				while(bucle1 == true) {
                	try { 
                		String nombre = sc.nextLine();
                		bucle1 = false;
                		nombreUsuario = nombre;
                	}catch(Exception e){
                		System.out.println(nombreUsuario + " no has introducido un nombre correcto");
                		sc.next();
                	}
                }
				
				while(bucle4 == true) {
					Scanner sc1 = new Scanner(System.in);
					respuestaUsuario = 0;
					numeroMaximo = 0;
					
					intentos = 0;
					System.out.println(nombreUsuario + "\nSi quieres jugar pulse --->1" + "\nSi no quieres jugar pulse --->2"+"\n/////////////////////////////////////////////////////////////////////////");
					int respuesta = sc.nextInt();
            		respuestaUsuario = respuesta;
            		if(respuesta == 2) {
            			bucle4 = false;
            			bucle2 = false;
                		bucle3 = false;
                		bucle1 = false;
            		}
            		if (respuesta == 1) {
            			bucle4 = false;
            			bucle2 = true;
            			bucle3 = true;
            			numeroRandom = 0;
            			
            			while(bucle2 == true) {
        					System.out.println(nombreUsuario + " introduce un numero para empezar el juego" + "\n/////////////////////////////////////////////////////////////////////////");
                        	try { 
                        		int numero = sc.nextInt();
                        		numeroMaximo = numero;
                        		
                        		if(numeroMaximo >=2) {
                        			bucle2 = false;
                        			//numeroRandom = (int) (Math.random() * numeroMaximo) + 1;
                        		}else{
                        			System.out.println(nombreUsuario + " introduce un numero mas grande" + "\n/////////////////////////////////////////////////////////////////////////");
                        			bucle2 = true;
                        		}
                        	}catch(Exception e){
                        		System.out.println(nombreUsuario + " no has introducido un numero correcto" + "\n/////////////////////////////////////////////////////////////////////////");
                        		sc.next();
                        	}
                        }

            			numeroRandom = (int) (Math.random() * numeroMaximo) + 1;
                        
                        while(bucle3 == true) {
                        	
                        	
                        	
        					System.out.println(nombreUsuario + " introduce un numero que este entre 1 y " + numeroMaximo);
                        	try { 
                        		int numero = sc.nextInt();
                        		numeroIntroducido = numero;
                        		if(numeroMaximo > numero && numero >= 1) {
                        			intentos += +1;
                        			numeroIntroducido = numero;
                        		}else {
                        			System.out.println(nombreUsuario + " introduce un numero entre 1 y el numero que has introducido ");
                        			bucle3 = true;
                        		}
                        		if(numeroIntroducido == numeroRandom) {
                        			bucle3 = false;
                        			bucle4 = true;
                        			System.out.println(nombreUsuario + "\n  _    _           _____        _____          _   _          _____   ____  \n"
                        					+ " | |  | |   /\\    / ____|      / ____|   /\\   | \\ | |   /\\   |  __ \\ / __ \\ \n"
                        					+ " | |__| |  /  \\  | (___       | |  __   /  \\  |  \\| |  /  \\  | |  | | |  | |\n"
                        					+ " |  __  | / /\\ \\  \\___ \\      | | |_ | / /\\ \\ | . ` | / /\\ \\ | |  | | |  | |\n"
                        					+ " | |  | |/ ____ \\ ____) |     | |__| |/ ____ \\| |\\  |/ ____ \\| |__| | |__| |\n"
                        					+ " |_|  |_/_/    \\_\\_____/       \\_____/_/    \\_\\_| \\_/_/    \\_\\_____/ \\____/ \n"
                        					+ "                                                                            \n"
                        					+ "                                                                            " + "\nLo has conseguido en: " + intentos + " intentos" );
                        			
                        			
                                    
                            		 	
                        		}else if(numeroIntroducido < numeroRandom){
                        			bucle3 = true;
                		 			System.out.println(nombreUsuario + " no has encontrado el numero secreto." + "\nVuelve a intentarlo con un numero mas grande, mucho animo." + "\nLlevas " + intentos + " intentos" +"\n/////////////////////////////////////////////////////////////////////////" );	
                        			}else if (numeroIntroducido > numeroRandom) {
                        		 			bucle3 = true;
                        		 			System.out.println(nombreUsuario + " no has encontrado el numero secreto." + "\nVuelve a intentarlo con un numero mas pequenno, mucho animo." + "\nLlevas " + intentos + " intentos" +"\n/////////////////////////////////////////////////////////////////////////");
                        			}
                        		
                        			
                        	}catch(Exception e){
                        		System.out.println(nombreUsuario + " no has introducido un numero correcto");
                        		sc.next();
                        	}	
                        }
            			
            		}if(respuestaUsuario != 1 && respuestaUsuario != 2 ){
            			System.out.println(nombreUsuario + " has introducido mal la respuesta");
            			bucle4 = true;
            		}		
				}
					
				                                     
        }
}